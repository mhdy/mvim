" create and flush autocmd group
augroup vimrc
  autocmd!
augroup END

" set filetype for special files
autocmd vimrc BufNewFile,BufRead .bashrc,.bash_* set filetype=sh
autocmd vimrc BufNewFile,BufRead ~/.ssh/config.d/* set filetype=sshconfig
autocmd vimrc BufNewFile,BufRead *.cls,*.sty,*.tex set filetype=tex
autocmd vimrc BufNewFile,BufRead /tmp/*mutt* set filetype=mail
autocmd vimrc BufNewFile,BufRead,BufEnter *.h set filetype=c

" turn on spell checking for specific file types
function! ActivateSpellChecking()
  setlocal spell spelllang=en_us
  syntax spell toplevel
endfunction
autocmd vimrc FileType text,plaintex,tex,markdown,mail call ActivateSpellChecking()

" strip trailing white space
function! StripTrailingWhiteSpace()
  if exists('b:StripWhiteSpace') && b:StripWhiteSpace == 0 | return | endif
  let l:wv = winsaveview()
  %s/\s\+$//e
  call winrestview(l:wv)
endfunction
autocmd vimrc FileType markdown let b:StripWhiteSpace = 0
autocmd vimrc BufWritePre * call StripTrailingWhiteSpace()

" remove empty lines at the end of file
function! RemoveTrailingEmptyLines()
  let l:wv = winsaveview()
  %s/\n\+\%$//e
  call winrestview(l:wv)
endfunction
autocmd vimrc BufWritePre * call RemoveTrailingEmptyLines()

" deactivate textwidth
autocmd vimrc FileType text,plaintex,tex,markdown set textwidth=0

" load session if any
function! LoadSession()
  if filereadable(".session.vim")
    source .session.vim
  endif
endfunction
autocmd vimrc VimEnter * nested call LoadSession()
