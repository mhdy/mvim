PREFIX := ~/.vim

clean:
	@echo nothing to do

clean-tree:
	rm -rf \
		$(PREFIX)/plugins/ \
		$(PREFIX)/compiler/ \
		$(PREFIX)/snips/ \
		$(PREFIX)/after/ftplugin/*.vim \
		$(PREFIX)/_*.vim \
		$(PREFIX)/vimrc

make-tree:
	mkdir -p \
		$(PREFIX)/ \
		$(PREFIX)/snips/ \
		$(PREFIX)/after/ftplugin/ \
		$(PREFIX)/compiler/ \
		$(PREFIX)/plugins/ \

install: clean-tree make-tree
	install -m 0600 vimrc $(PREFIX)/
	install -m 0600 _*.vim $(PREFIX)/
	install -m 0600 plugins/_*.vim $(PREFIX)/plugins/
	cp snips/*.snippets $(PREFIX)/snips/
	cp after/ftplugin/*.vim $(PREFIX)/after/ftplugin/
	cp compiler/*.vim $(PREFIX)/compiler/

uninstall: clean-tree
	@echo remove manually

.PHONY: clean-tree clean make-tree install uninstall
