" plugins with vim-plug
if ! filereadable(system('echo -n "$HOME/.vim/autoload/plug.vim"'))
	echo 'Downloading vim-plug...'
	silent !curl --create-dirs -f -L
        \ -o $HOME/.vim/autoload/plug.vim
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall
endif
call plug#begin('~/.vim/plugged')
source ~/.vim/_plugins.vim
call plug#end()

" general settings
source ~/.vim/_settings.vim

" status line
source ~/.vim/_statusline.vim

" theme
source ~/.vim/_theme.vim

" autocmd
source ~/.vim/_autocmd.vim

" plugin settings
source ~/.vim/plugins/_nerdtree.vim
source ~/.vim/plugins/_fzf.vim
source ~/.vim/plugins/_emmet.vim
source ~/.vim/plugins/_ultisnips.vim
source ~/.vim/plugins/_tagbar.vim
source ~/.vim/plugins/_easymotion.vim
source ~/.vim/plugins/_system-copy.vim
source ~/.vim/plugins/_quickscope.vim

" mappings
source ~/.vim/_mappings.vim

" misc
source ~/.vim/_misc.vim
