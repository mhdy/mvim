function! GetGitBranch()
  let l:branch = FugitiveHead()
  if l:branch == ''
    return ''
  else
    return '[' . l:branch . ']'
  endif
endfunction

function! GetMode()
  let l:modes = {
    \ 'n': 'normal',
    \ 'v': 'visual', 'V': 'visual',
    \ 's': 'select', 'S': 'select',
    \ 'i': 'insert',
    \ 'R': 'replace'
  \ }
  let l:currentmode = mode()
  let l:mode = get(l:modes, l:currentmode, 'unknown')
  if mode == 'unknown'
    let l:mode = l:currentmode
  else
    let l:mode = toupper(l:mode)
  endif
  if mode == 'INSERT'
    if &paste == 1
      let l:mode .= ' (paste)'
    endif
  endif
  return '[' . l:mode . ']'
endfunction

set laststatus=2
set statusline=
set statusline=%<
set statusline+=%{GetMode()}\  " mode indicator
set statusline+=%(%{GetGitBranch()}\ %)  " branch name
set statusline+=%F\  " relative path to the file in the buffer.
set statusline+=%=
set statusline+=[%n]\  " modified flag
set statusline+=%(%m\ %) " modified flag
set statusline+=%(%r\ %) " readonly flag, text is [ro].
set statusline+=%(%y\ %) " type of file in the buffer, e.g. [vim]
set statusline+=[%{&fileformat},\ %{strlen(&fenc)?&fenc:&enc}]\  " encoding
set statusline+=[%l/%L:%c\ (%p%%)] " line and column
