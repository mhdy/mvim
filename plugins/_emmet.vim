let g:user_emmet_mode = 'i' " only enable insert mode functions
let g:user_emmet_install_global = 0
let g:user_emmet_settings = {
  \ 'javascriptreact': {
    \ 'extends': 'jsx',
  \ },
  \ 'typescriptreact': {
    \ 'extends': 'jsx',
  \ },
  \ 'javascript': {
    \ 'extends': 'jsx',
  \ },
  \ 'typescript': {
    \ 'extends': 'jsx',
  \ },
\ }
augroup emmet
  autocmd!
  autocmd FileType html,htmldjango,css,typescript,typescriptreact,javascriptreact,markdown EmmetInstall
augroup END
