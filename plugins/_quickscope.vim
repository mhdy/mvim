let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']
let g:qs_max_chars = 512

" colors
augroup quickscope
  autocmd!
  autocmd ColorScheme * highlight QuickScopePrimary ctermfg=9 cterm=underline
  autocmd ColorScheme * highlight QuickScopeSecondary ctermfg=12 cterm=underline
augroup END
