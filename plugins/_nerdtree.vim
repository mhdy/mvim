let NERDTreeShowHidden = 1
let NERDTreeWinSize = 35
let NERDTreeMinimalUI = 1
let NERDTreeMinimalMenu = 1
let NERDTreeRespectWildIgnore = 1
let NERDTreeMapJumpNextSibling = "<nop>"
let NERDTreeMapJumpPrevSibling = "<nop>"
let NERDTreeIgnore = ['\.git$']

" --- latex
let NERDTreeIgnore += [
  \ '\.aux$',
  \ '\.fdb_latexmk$',
  \ '\.fls$',
  \ '\.lof$',
  \ '\.log$',
  \ '\.lot$',
  \ '\.out$',
  \ '\.synctex$',
  \ '\.toc$',
  \ '\.fmt',
  \ '\.snm',
  \ '\.nav',
  \ '\.bbl$',
  \ '\.bcf$',
  \ '\.blg$',
  \ '\-blx.aux$',
  \ '\-blx.bib$',
  \ '\.run.xml$',
\ ]

" --- python
let NERDTreeIgnore += [
  \ '^__pycache__$',
  \ '\.py[cod]$',
  \ '\.ipynb_checkpoints',
\ ]

" --- C
let NERDTreeIgnore += [
  \ '\.o$',
  \ '\.ko$',
  \ '\.obj$',
  \ '\.elf$',
  \ '\.exe$',
  \ '\.out$',
  \ '\.app$',
  \ '\.i[0-9]86$',
  \ '\.x86_64$',
  \ '\.hex$',
  \ '\.ilk$',
  \ '\.map$',
  \ '\.exp$',
  \ '\.gch$',
  \ '\.pch$',
  \ '\.lib$',
  \ '\.a$',
  \ '\.la$',
  \ '\.lo$',
\ ]
