let g:system_copy#copy_command = 'xclip -selection clipboard'
let g:system_copy#paste_command = 'xclip -selection clipboard -out'
let g:system_copy_silent = 1
