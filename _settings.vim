set nocompatible " not compatible with old vi
set t_Co=256 " number of terminal colors
syntax enable " enable syntax highlighting
filetype plugin indent on " enable filetype-based plugins and indentation
set autoread
set backspace=indent,eol,start " backspacing over everything in insert mode
set belloff=all
set display=lastline
set encoding=utf-8 " set utf-8 as standard encoding
set formatoptions=cqnj
set hidden
set incsearch " makes search act like search in modern browsers
set nojoinspaces " prevent two spaces after a '.', '?' and '!' when a joining
set shortmess+=I " disable intro message
set nostartofline
set ttimeoutlen=50
set timeoutlen=300
set ttyfast
set title " update the WM_NAME depending on what file is open
set noshowmode " disable displaying mode indicator
set viminfo='50,<1000,s100 " register sizes (see :h 'vi)
set ignorecase " ignore case when searching
set smartcase " don't ignore case when used while searching
set autoindent smartindent " auto indentation
set textwidth=79 colorcolumn=+1 " maximum width of text that is being inserted
set linebreak
set number relativenumber " display line numbers
set cursorline " show the cursor line and column
set showmatch " show matching brackets when text indicator is over them
set showcmd " show vim command
set wildmenu " command-line completion enhanced
set scrolloff=7 " number of screen lines to keep above and below the cursor

" turn off backup
set noswapfile
set nobackup
set nowritebackup

" tabs
set tabstop=2 softtabstop=2
set shiftwidth=2
set expandtab " replace tab with spaces
