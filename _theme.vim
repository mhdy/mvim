set background=dark
let g:gruvbox_material_background = 'hard'
let g:gruvbox_material_better_performance = 1
colorscheme gruvbox-material
highlight EndOfBuffer ctermfg=black
if &diff
  colorscheme gruvbox
endif
