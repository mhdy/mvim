" toggle quickfix
function! ToggleQuickfix()
  let n1 = winnr("$")
  copen
  let n2 = winnr("$")
  if n1 == n2
    cclose
  endif
endfunction

" open help in vertical split
cnoreabbrev H vertical botright help

" mvl
function! Copy(src, dst)
  if !empty(a:dst)
    silent execute '!cp ' . a:src . ' ' . a:dst
  endif
endfunction
augroup mvl
  autocmd!
  autocmd BufWritePost */dotfiles/.config/sxhkd/sxhkdrc
    \ :call Copy(expand('%:p'), '~/.config/sxhkd/sxhkdrc')
    \ | silent execute system("pkill -USR1 sxhkd")
  autocmd BufWritePost */dotfiles/.bookmarks
    \ :call Copy(expand('%:p'), '~/.bookmarks')
  autocmd BufWritePost */dotfiles/.bash_profile
    \ :call Copy(expand('%:p'), '~/.bash_profile')
  autocmd BufWritePost */dotfiles/.bash_profile_local
    \ :call Copy(expand('%:p'), '~/.bash_profile_local')
  autocmd BufWritePost */dotfiles/.bash_aliases
    \ :call Copy(expand('%:p'), '~/.bash_aliases')
  autocmd BufWritePost */dotfiles/.bash_local
    \ :call Copy(expand('%:p'), '~/.bash_local')
  autocmd BufWritePost */dotfiles/.bashrc
    \ :call Copy(expand('%:p'), '~/.bashrc')
  autocmd BufWritePost */dotfiles/.xinitrc
    \ :call Copy(expand('%:p'), '~/.xinitrc')
augroup END
