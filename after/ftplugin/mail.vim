" if exists("b:did_ftplugin") | finish | endif
" let b:did_ftplugin = 1 " do not load twice in one buffer

set noautoindent
set textwidth=0

function! FixMailList()
  let cursor_pos = getpos('.')
  for e in ["To", "Cc", "Bcc"]
    call search("^" . e . ": ")
    silent! execute("g/^". e . ": /.,/^[A-Z]/-1 join")
    silent! s/[,;]\s*$//g
    if match(getline('.'), e . ': .*[,;] \+[a-zA-Z]') != -1
      silent! s/ *[,;] */,\r  /g
    endif
  endfor
  call setpos('.', cursor_pos)
endfunction
autocmd BufWritePre,VimLeave * call FixMailList()

nnoremap <buffer> <leader>t /^To:<cr>A<space>
nnoremap <buffer> <leader>c /^Cc:<cr>A<space>
nnoremap <buffer> <leader>b /^Bcc:<cr>A<space>
nnoremap <buffer> <leader>s /^Subject:<cr>A<space>
nnoremap <buffer> <leader>r /^Reply-To:<cr>A<space>
nnoremap <buffer> <leader>b Go
