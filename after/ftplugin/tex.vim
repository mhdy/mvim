" if exists("b:did_ftplugin") | finish | endif
" let b:did_ftplugin = 1 " do not load twice in one buffer

let g:tex_flavor='latex' " avoid plaintex

compiler tex

function! Compile()
  write
  silent Make
  cwindow
  redraw!
endfunction

function! OpenPDF()
  let s:prefix = "/workspace"
  let s:path = expand('%')
  let s:synctexarg = line('.') . ":" . col('.') . ":" . s:prefix . "/" . s:path
  let s:cmd = "!zathura --synctex-forward " . s:synctexarg . " main.pdf & disown"
  silent exec s:cmd
  redraw!
endfunction

function! CleanUpCWD()
  if filereadable("main.tex") && filereadable("main.synctex")
    silent exec "!mtex clean"
  endif
endfunction

autocmd VimLeave * silent call CleanUpCWD()

nnoremap <buffer> <leader>cc :call Compile()<cr>
inoremap <buffer> <leader>cc <esc>:call Compile()<cr>
nnoremap <buffer> <leader>z :call OpenPDF()<cr>

vnoremap <buffer> <leader>b c\textbf{<esc>pa}<esc>
vnoremap <buffer> <leader>i c\textit{<esc>pa}<esc>
vnoremap <buffer> <leader>t c\text{<esc>pa}<esc>
vnoremap <buffer> <leader>q c``<esc>pa''<esc>
vnoremap <buffer> <leader>" c``<esc>pa''<esc>
vnoremap <buffer> <leader>< c<<esc>pa><esc>
vnoremap <buffer> <leader>( c(<esc>pa)<esc>
vnoremap <buffer> <leader>{ c{<esc>pa}<esc>
vnoremap <buffer> <leader>$ c\(<esc>pa\)<esc>
vnoremap <buffer> <leader>i c\begin{itemize}<cr>\end{itemize}<esc>kp`[v`]:norm I \item <esc>gvOkoj=
vnoremap <buffer> <leader>e c\begin{enumerate}<cr>\end{enumerate}<esc>kp`[v`]:norm I \item <esc>gvOkoj=
vnoremap <buffer> <leader>m c\(<esc>pa\)<esc>
