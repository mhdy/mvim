" if exists("b:did_ftplugin") | finish | endif
" let b:did_ftplugin = 1 " do not load twice in one buffer

function! GetPrompt()
  let l:cmd  = "docker run -it --rm "
  let l:cmd .= "-v \"" . getcwd() . ":/workspace\" "
  let l:cmd .= "-w /workspace "
  let l:cmd .= "rust:1.52.1 /bin/bash"
  execute "vertical botright terminal " . l:cmd
endfunction
nnoremap <buffer> <leader><cr> :call GetPrompt()<cr>
