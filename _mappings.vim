let mapleader = ','

" plugins mappings
" fugitive
nnoremap <leader>gs :Git<cr>/^$<cr>j
nnoremap <leader>gp :Git push<cr>
" fzf
nnoremap <leader>ff :FZF<cr>
nnoremap <leader>fr :Rg<cr>
nnoremap <leader>fb :Buffers<cr>
nnoremap <leader>fs :Snippets<cr>
inoremap <leader>fs <esc>:Snippets<cr>
" nerdtree
nnoremap <leader>n :NERDTreeToggle<cr>
" tagbar
nnoremap <leader>t :TagbarToggle<cr>
" commentary
nnoremap <leader>c :Commentary<cr>
vnoremap <leader>c :Commentary<cr>
" emmet
imap <leader>e <C-y>,
" easymotion
map <leader>m <Plug>(easymotion-s)
" ultisnips
let g:UltiSnipsExpandTrigger = '<leader><tab>'
let g:UltiSnipsJumpForwardTrigger = '<leader>n'
let g:UltiSnipsJumpBackwardTrigger = '<leader>N'
" system copy
imap <leader>p <esc><Plug>SystemPasteLine
nmap <leader>p <Plug>SystemPasteLine

" leader mappings
" join and split lines
vnoremap <leader>j J
vnoremap <leader>s gq
nnoremap <leader>s gql
vnoremap <leader>js J<esc>gql
" remove highlight
nnoremap <leader>/ :noh<cr>
" sed
nnoremap <leader>S :%s///g<left><left><left>
vnoremap <leader>S :s///g<left><left><left>
" select all
nnoremap <leader>V ggVG
" yank all
nnoremap <leader>Y ggyG
" sort lines of a paragraph
nnoremap <leader>^ {jV}k:sort<cr>
vnoremap <leader>^ :sort<cr>
" format buffer
nnoremap <leader>= magg=G`a
" merge conflict: vim fugitive
nnoremap <leader>gh :diffget //2<cr>
nnoremap <leader>gl :diffget //3<cr>
" merge conflicts: git mergetool
nnoremap <leader>gv :diffget LOCAL<cr>
nnoremap <leader>gb :diffget BASE<cr>
nnoremap <leader>gn :diffget REMOTE<cr>
" write/quit (avoid semi colon)
nnoremap <leader>w :w<cr>
nnoremap <leader>x :wq<cr>
nnoremap <leader>q :q<cr>
nnoremap <leader><esc> :qa!<cr>
" spell language
nnoremap <leader>1 :setlocal spell spelllang=en_us<cr>
nnoremap <leader>2 :setlocal spell spelllang=fr<cr>
nnoremap <leader>0 :setlocal nospell<cr>
" toggle quickfix
nnoremap <leader><space> :call ToggleQuickfix()<cr>
" session
nnoremap <leader>ss :mksession! .session.vim<cr>
nnoremap <leader>sl :call LoadSession()<cr>

" non-leader mappings
" make Y consistent with C and D
nnoremap Y y$
" better visual indentation
vnoremap < <gv
vnoremap > >gv
" remap escape
inoremap kj <esc><right>
inoremap KJ <esc><right>
inoremap jk <right>
inoremap JK <right>
" move lines up or down
vnoremap <C-k> :move '<-2<cr>gv=gv
vnoremap <C-j> :move '>+1<cr>gv=gv
" move around buffers
nnoremap <C-k> <C-w>W
nnoremap <C-j> <C-w>w
" move around tabs
nnoremap <C-l> gt
nnoremap <C-h> gT
nnoremap [[ :tabmove -1<cr>
nnoremap ]] :tabmove +1<cr>
" [in/de]crease split size
nnoremap <C-w>] <C-w>5>
nnoremap <C-w>[ <C-w>5<
" navigate long lines like separate lines
nnoremap j gj
nnoremap k gk
" help
nnoremap ?? K
" go right/left
nnoremap L $
nnoremap H ^
" scroll buffer then center
nnoremap <C-u> <C-u>zz
nnoremap <C-d> <C-d>zz
" center next search match
nnoremap n nzz
nnoremap N Nzz
" add empty line above and/or below
nnoremap -- 2o<esc>:-1,.s/.*//<cr>i
nnoremap __ 2O<esc>:-1,.s/.*//<cr>ki
nnoremap _- o<esc>:s/.*//<cr>kO<esc>:s/.*//<cr>ji
" extending *i? and *a?
let items = [':', ',', ';', '/', '.', '*', '_', '~', '=', '-', '+', '$', '#', '%', '&', '\', '<bar>']
for item in items
  exec 'nnoremap yi'.item.' T'.item.'yt'.item
  exec 'nnoremap ya'.item.' F'.item.'yf'.item
  exec 'nnoremap ci'.item.' T'.item.'ct'.item
  exec 'nnoremap ca'.item.' F'.item.'cf'.item
  exec 'nnoremap di'.item.' T'.item.'dt'.item
  exec 'nnoremap da'.item.' F'.item.'df'.item
  exec 'nnoremap vi'.item.' T'.item.'vt'.item
  exec 'nnoremap va'.item.' F'.item.'vf'.item
  exec 'nmap cpi'.item.' T'.item.'cpt'.item
  exec 'nmap cpa'.item.' F'.item.'cpf'.item
endfor
nnoremap <esc> <esc>
